import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { throwError } from 'rxjs/internal/observable/throwError';
import { tap } from 'rxjs/operators';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';

@Injectable({ providedIn: 'root' })
export class WebsocketService {
  isConnected: boolean;
  id: string;
  model: number;

  socket: WebSocketSubject<any>;
  connected = new Subject<boolean>();

  connect(id: string, model: number, host: string) {
    if (this.isConnected) {
      return throwError('Already connected');
    }
    this.socket = webSocket(host);
    this.isConnected = true;
    this.id = id;
    this.model = model;
    this.socket.next({ hello: id, model });
    this.connected.next(true);

    return this.socket.pipe(
      tap({
        error: console.log,
        complete: () => (this.isConnected = false)
      })
    );
  }

  disconnect() {
    this.socket.next({ bye: this.id });
    this.connected.next(false);
    this.isConnected = false;
    this.socket.complete();
  }

  onConnection(): Observable<boolean> {
    return this.connected.asObservable();
  }

  getSocket() {
    return this.socket.asObservable();
  }

  move(m: any) {
    m.id = this.id;
    m.update = true;
    this.sendMessage(m);
  }

  stop() {
    this.sendMessage({
      id: this.id,
      update: true,
      stop: true
    });
  }

  private sendMessage(message: any) {
    if (!this.isConnected) {
      return;
    }
    this.socket.next(message);
  }
}
