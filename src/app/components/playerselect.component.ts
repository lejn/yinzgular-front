import { Component, Input } from '@angular/core';

@Component({
  selector: 'yinz-player-select',
  templateUrl: './playerselect.component.html'
})
export class PlayerSelectComponent {
  @Input() src: string;
}
