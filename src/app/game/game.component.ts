import { AfterViewInit, Component } from '@angular/core';
import { WebsocketService } from '../websocket.service';
import { config } from './config';
import { MainScene } from './scenes/main';

@Component({
  selector: 'yinz-game',
  templateUrl: './game.component.html'
})
export class GameComponent implements AfterViewInit {
  game: Phaser.Game;

  constructor(private readonly socket: WebsocketService) {}

  ngAfterViewInit(): void {
    this.game = new Phaser.Game(config);
    this.socket.onConnection().subscribe(connected => {
      if (connected) {
        this.game.scene.add(MainScene.SCENE_ID, MainScene, true, {
          id: this.socket.id,
          socket: this.socket,
          model: this.socket.model
        });
      } else {
        this.game.scene.remove(MainScene.SCENE_ID);
      }
    });
  }
}
