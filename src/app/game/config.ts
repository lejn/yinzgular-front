export const config: Phaser.Types.Core.GameConfig = {
  title: 'Yinzgular',
  version: '0.1',
  type: Phaser.AUTO,
  physics: {
    default: 'arcade',
    arcade: {
      debug: false
    }
  },
  input: {
    keyboard: true
  },
  backgroundColor: '#FFFFFF',
  render: { pixelArt: true },
  scale: {
    mode: Phaser.Scale.FIT,
    parent: 'game',
    autoCenter: Phaser.Scale.CENTER_BOTH
  }
};

export const refreshPeriod = 50;
