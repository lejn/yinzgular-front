import { Subject } from 'rxjs';

export class Player {
  private SCALE = 2;
  private WALKING_SPEED = 100;
  private MAX_HEALTH = 100;
  private health = this.MAX_HEALTH;
  private isMoving = false;

  moving = new Subject();
  stopping = new Subject();

  // phaser stuff
  private container: Phaser.GameObjects.Container;
  private body: Phaser.GameObjects.Sprite;
  private head: Phaser.GameObjects.Sprite;
  private red: Phaser.GameObjects.Sprite;
  private green: Phaser.GameObjects.Sprite;
  private cursors: Phaser.Types.Input.Keyboard.CursorKeys;

  constructor(
    scene: Phaser.Scene,
    x: number,
    y: number,
    private model: number,
    private id: string
  ) {
    this.container = scene.add.container(x, y);

    // body
    this.body = scene.add.sprite(0, 0, 'body');
    this.container.add(this.body);

    // head
    this.head = scene.add.sprite(0, -18, 'heads', model);
    this.container.add(this.head);

    // name
    const label = scene.add.text(0, 20, id, {
      fontSize: 20
    });
    label.setOrigin(0.5);
    label.scale = 0.4;
    this.container.add(label);

    // healthbar
    this.red = scene.add.sprite(0, -30, 'healthbar', 0);
    this.green = scene.add.sprite(-12, -30, 'healthbar', 1);
    this.green.setOrigin(0, 0.5);
    this.container.add(this.red);
    this.container.add(this.green);

    this.cursors = scene.input.keyboard.createCursorKeys();
    scene.physics.add.existing(this.container);
    this.container.setInteractive();

    scene.tweens.add({
      targets: [this.head],
      y: -17,
      ease: 'Cubic',
      duration: 500,
      yoyo: true,
      repeat: -1,
      callbackScope: this
    });

    scene.anims.create({
      key: 'stand',
      frames: scene.anims.generateFrameNumbers('body', {
        frames: [0, 1]
      }),
      frameRate: 2,
      repeat: -1
    });

    scene.anims.create({
      key: 'run',
      frames: scene.anims.generateFrameNumbers('running', {
        frames: [0, 1]
      }),
      frameRate: 8,
      repeat: -1
    });

    this.container.scale = this.SCALE;
    this.head.scale = 0.7;
  }

  flip(flip: boolean) {
    this.body.setFlipX(flip);
    this.head.setFlipX(flip);
  }

  destroy() {
    this.container.destroy();
    this.moving.complete();
    this.stopping.complete();
  }

  moveRemote(msg: any) {
    if (msg.direction === 'left') {
      this.flip(true);
    } else if (msg.direction === 'right') {
      this.flip(false);
    }
    this.container.x = msg.x;
    this.container.y = msg.y;
    this.isMoving = true;
  }

  stopRemote() {
    this.isMoving = false;
  }

  damage(dealt: number) {
    if (this.health <= 0) {
      return;
    }
    this.health -= dealt;
    this.green.scaleX = this.health / this.MAX_HEALTH;
  }

  update(handleInput = true): void {
    if (handleInput) {
      this.handleKeyboardInput();
    }
    this.handleAnimations();
  }

  private handleAnimations(): void {
    const player: Phaser.Physics.Arcade.Body = this.container
      .body as Phaser.Physics.Arcade.Body;

    if (player.velocity.x > 0) {
      this.flip(false);
    } else if (player.velocity.x < 0) {
      this.flip(true);
    }

    if (this.isMoving) {
      this.body.play('run', true);
    } else {
      this.body.play('stand', true);
    }
  }

  private handleKeyboardInput(): void {
    const player: Phaser.Physics.Arcade.Body = this.container
      .body as Phaser.Physics.Arcade.Body;
    const speed = this.WALKING_SPEED;
    player.setVelocity(0);

    if (this.cursors.left.isDown) {
      player.setVelocityX(-speed);
      this.move('left');
    } else if (this.cursors.right.isDown) {
      player.setVelocityX(speed);
      this.move('right');
    }

    if (this.cursors.up.isDown) {
      player.setVelocityY(-speed);
      this.move('up');
    } else if (this.cursors.down.isDown) {
      player.setVelocityY(speed);
      this.move('down');
    }

    if (this.isMoving && player.velocity.x === 0 && player.velocity.y === 0) {
      this.stop();
    }
  }

  private move(direction: string) {
    this.isMoving = true;
    this.moving.next({
      direction,
      model: this.model,
      x: this.container.x,
      y: this.container.y
    });
  }

  private stop() {
    this.isMoving = false;
    this.stopping.next();
  }
}
