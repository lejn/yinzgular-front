import { Player } from './player';

export class CAlcober extends Player {
  constructor(scene: Phaser.Scene, x: number, y: number, id: string) {
    super(scene, x, y, 10, id);
  }
}

export class Adrian extends Player {
  constructor(scene: Phaser.Scene, x: number, y: number, id: string) {
    super(scene, x, y, 1, id);
  }
}

export class Borja extends Player {
  constructor(scene: Phaser.Scene, x: number, y: number, id: string) {
    super(scene, x, y, 2, id);
  }
}

export class Nacho extends Player {
  constructor(scene: Phaser.Scene, x: number, y: number, id: string) {
    super(scene, x, y, 3, id);
  }
}

export class Jimmy extends Player {
  constructor(scene: Phaser.Scene, x: number, y: number, id: string) {
    super(scene, x, y, 4, id);
  }
}

export class Fidel extends Player {
  constructor(scene: Phaser.Scene, x: number, y: number, id: string) {
    super(scene, x, y, 5, id);
  }
}

export class AHerrero extends Player {
  constructor(scene: Phaser.Scene, x: number, y: number, id: string) {
    super(scene, x, y, 6, id);
  }
}

export class DAcedos extends Player {
  constructor(scene: Phaser.Scene, x: number, y: number, id: string) {
    super(scene, x, y, 7, id);
  }
}

export class CZaragoza extends Player {
  constructor(scene: Phaser.Scene, x: number, y: number, id: string) {
    super(scene, x, y, 8, id);
  }
}

export class Pipo extends Player {
  constructor(scene: Phaser.Scene, x: number, y: number, id: string) {
    super(scene, x, y, 11, id);
  }
}

export class John extends Player {
  constructor(scene: Phaser.Scene, x: number, y: number, id: string) {
    super(scene, x, y, 12, id);
  }
}

export class Javi extends Player {
  constructor(scene: Phaser.Scene, x: number, y: number, id: string) {
    super(scene, x, y, 0, id);
  }
}

export class Fede extends Player {
  constructor(scene: Phaser.Scene, x: number, y: number, id: string) {
    super(scene, x, y, 13, id);
  }
}
export class Jacobo extends Player {
  constructor(scene: Phaser.Scene, x: number, y: number, id: string) {
    super(scene, x, y, 14, id);
  }
}
export class Israel extends Player {
  constructor(scene: Phaser.Scene, x: number, y: number, id: string) {
    super(scene, x, y, 15, id);
  }
}
export class Jonathan extends Player {
  constructor(scene: Phaser.Scene, x: number, y: number, id: string) {
    super(scene, x, y, 16, id);
  }
}
export class Cesar extends Player {
  constructor(scene: Phaser.Scene, x: number, y: number, id: string) {
    super(scene, x, y, 17, id);
  }
}

export function getPlayerName(type: number): string {
  switch (type) {
    case 0:
      return 'Thogguran';
    case 1:
      return 'Prom Queen';
    case 2:
      return 'Excuse Me';
    case 3:
      return 'Shotha';
    case 4:
      return 'Welcome';
    case 5:
      return 'Ub-Voonarth';
    case 6:
      return 'Sathl-Nyarla';
    case 7:
      return 'MonkeyButler91';
    case 8:
      return 'Vacaciones En La Playa';
    case 10:
      return 'Voonyarlei';
    case 11:
      return 'Pee-Po';
    case 12:
      return `Mahma ibn Khy'zi al-Arthati`;
    case 13:
      return 'Atila';
    case 14:
      return 'Cookie Dough';
    case 15:
      return 'Canela N Ramah';
    case 16:
      return 'Kelly Clarkson';
    case 17:
      return 'Bon Jovi';
  }
}

export function playerBuilder(
  scene: Phaser.Scene,
  x: number,
  y: number,
  id: string,
  type: number
): Player {
  switch (type) {
    case 0:
      return new Javi(scene, x, y, id);
    case 1:
      return new Adrian(scene, x, y, id);
    case 2:
      return new Borja(scene, x, y, id);
    case 3:
      return new Nacho(scene, x, y, id);
    case 4:
      return new Jimmy(scene, x, y, id);
    case 5:
      return new Fidel(scene, x, y, id);
    case 6:
      return new AHerrero(scene, x, y, id);
    case 7:
      return new DAcedos(scene, x, y, id);
    case 8:
      return new CZaragoza(scene, x, y, id);
    case 10:
      return new CAlcober(scene, x, y, id);
    case 11:
      return new Pipo(scene, x, y, id);
    case 12:
      return new John(scene, x, y, id);
    case 13:
      return new Fede(scene, x, y, id);
    case 14:
      return new Jacobo(scene, x, y, id);
    case 15:
      return new Israel(scene, x, y, id);
    case 16:
      return new Jonathan(scene, x, y, id);
    case 17:
      return new Cesar(scene, x, y, id);
  }
}
