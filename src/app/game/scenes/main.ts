import { Subscription } from 'rxjs';
import { throttleTime } from 'rxjs/operators';
import { WebsocketService } from '../../websocket.service';
import { Player } from '../objects/player';
import { playerBuilder } from '../objects/player-list';

export interface SceneData {
  id: string;
  socket: WebsocketService;
  model: number;
}

export class MainScene extends Phaser.Scene {
  static SCENE_ID = 'GAME_SCENE';
  socket: WebsocketService;
  connectionSub: Subscription;
  players: { [id: string]: Player };

  background: Phaser.GameObjects.Image;
  dunksText: Phaser.GameObjects.Text;

  dunks: number;
  player: Player;
  id: string;
  model: number;

  constructor() {
    super({
      key: MainScene.SCENE_ID
    });
  }

  preload(): void {
    this.load.spritesheet('body', './assets/body1-standing-48x24x2.png', {
      frameWidth: 24,
      frameHeight: 24
    });
    this.load.spritesheet('running', './assets/body1-running-48x24x2.png', {
      frameWidth: 24,
      frameHeight: 24
    });
    this.load.spritesheet('healthbar', './assets/healthbar-48x24x2.png', {
      frameWidth: 24,
      frameHeight: 24
    });

    this.load.spritesheet('heads', './assets/heads.png', {
      frameWidth: 48,
      frameHeight: 48
    });
  }

  init(data: SceneData): void {
    this.players = {};
    this.socket = data.socket;
    this.id = data.id;
    this.model = data.model;
    this.dunks = 0;

    this.connectionSub = this.socket
      .getSocket()
      .subscribe(this.processMessage.bind(this));
  }

  create(): void {
    const p = playerBuilder(
      this,
      this.sys.canvas.width / 2,
      this.sys.canvas.height / 2,
      this.id,
      this.model
    );

    p.moving.pipe(throttleTime(20)).subscribe(data => {
      this.socket.move(data);
    });

    p.stopping.subscribe(() => {
      this.socket.stop();
    });

    this.players[this.id] = p;
    this.player = p;

    setInterval(() => {
      p.damage(1);
    }, 200);

    // create texts
    this.dunksText = this.add.text(20, 20, `Dunks: ${this.dunks}`, {
      fontFamily: 'Connection',
      fontSize: 28,
      stroke: '#aaa',
      strokeThickness: 5,
      fill: '#000000'
    });
  }

  update(): void {
    for (const playerID of Object.keys(this.players)) {
      if (playerID === this.id) {
        this.players[playerID].update(true);
      } else {
        if (this.players[playerID]) {
          this.players[playerID].update(false);
        }
      }
    }
  }

  private processMessage(msg: any) {
    if (msg.id && msg.id === this.id) {
      return;
    }
    if (msg.hello && msg.hello !== this.id) {
      this.meetNewPlayer(msg.hello, msg.model);
    } else if (msg.update) {
      if (this.isKnownPlayer(msg.id)) {
        // animate remote player
        if (msg.stop) {
          this.players[msg.id].stopRemote();
        } else {
          this.players[msg.id].moveRemote(msg);
        }
      } else {
        // unknown player, get to know it
        this.meetNewPlayer(msg.id, msg.model);
      }
    } else if (msg.bye) {
      this.players[msg.bye].destroy();
      this.players[msg.bye] = undefined;
    } else if (msg.update) {
      console.log(`update from ${msg.id}`);
    }
  }

  private isKnownPlayer(id: string): boolean {
    return !!this.players[id];
  }

  private meetNewPlayer(id: string, model: number) {
    const p = playerBuilder(
      this,
      this.sys.canvas.width / 2,
      this.sys.canvas.height / 2,
      id,
      model
    );
    this.players[id] = p;
  }
}
