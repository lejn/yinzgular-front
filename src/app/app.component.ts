import { Component } from '@angular/core';
import 'phaser';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';
import { getPlayerName } from './game/objects/player-list';
import { WebsocketService } from './websocket.service';

@Component({
  selector: 'yinz-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  $connected: Observable<boolean>;
  playerName: string;
  serverURL = environment.yinzServer;
  generatedPlayerName: string;
  logger = '';
  messageId = 0;
  selectedHead: number;
  selectedHeadName: string;

  constructor(private readonly socket: WebsocketService) {
    this.generatedPlayerName = this.generatePlayerName();
    this.$connected = this.socket.onConnection();
  }

  connect() {
    let id: string;
    if (this.playerName) {
      id = this.playerName;
    } else {
      id = this.generatedPlayerName;
      this.playerName = id;
    }

    this.socket
      .connect(id, this.selectedHead, `ws://${this.serverURL}/ws`)
      .subscribe(this.processMessage.bind(this));
  }

  disconnect() {
    this.socket.disconnect();
    console.log('You left the game');
  }

  selectHead(e: MouseEvent) {
    const row = Math.floor(e.offsetY / 48);
    const col = Math.floor(e.offsetX / 48);
    this.selectedHead = row * 10 + col;
    this.selectedHeadName = getPlayerName(this.selectedHead);
    console.log(this.selectedHead);
  }

  private processMessage(msg: any) {
    if (msg.hello) {
      if (
        msg.hello === this.generatedPlayerName ||
        msg.hello === this.playerName
      ) {
        console.log(
          `Connected as <${this.playerName || this.generatedPlayerName}>`
        );
      } else {
        console.log(`Player <${msg.hello}> entered the game`);
      }
    } else if (msg.bye) {
      console.log(`Player <${msg.bye}> left the game`);
    } else {
      // console.log(JSON.stringify(msg));
    }
  }

  private generatePlayerName(): string {
    return Math.random()
      .toString(36)
      .substr(2, 4);
  }
}
