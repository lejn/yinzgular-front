import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule
} from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { PlayerSelectComponent } from './components/playerselect.component';
import { GameComponent } from './game/game.component';
import { MainScene } from './game/scenes/main';
import { WebsocketService } from './websocket.service';

@NgModule({
  declarations: [AppComponent, GameComponent, PlayerSelectComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule
  ],
  providers: [MainScene, WebsocketService],
  bootstrap: [AppComponent]
})
export class AppModule {}
